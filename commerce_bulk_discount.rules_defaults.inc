<?php

/**
 * @file
 * Default rules configurations for Commerce Bulk Discount.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_bulk_discount_default_rules_configuration() {

  $rules = array();

  // When an order's status is updating to "Shopping cart" from any other
  // status, all bulk discount (or surcharge) line items will be deleted. This
  // captures any cart contents change via Add to Cart forms / the Shopping cart
  // Views form. It also happens when the customer cancels out of the checkout
  // form.
  $rule = rules_reaction_rule();

  $rule->label = t('Delete all bulk discount (or surcharge) line items on shopping cart updates');
  $rule->active = TRUE;

  $rule
    ->event('commerce_order_update')
    ->condition('data_is', array(
      'data:select' => 'commerce-order:status',
      'op' => '==',
      'value' => 'cart',
    ))
    ->condition(rules_condition('data_is', array(
      'data:select' => 'commerce-order-unchanged:status',
      'op' => '==',
      'value' => 'cart',
    ))->negate())
    ->action('commerce_bulk_discount_delete_bulk_discount_line_items', array(
      'commerce_order:select' => 'commerce-order',
    ));

  $rules['commerce_bulk_discount_cart_update_delete'] = $rule;

  return $rules;
}
