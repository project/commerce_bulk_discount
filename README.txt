INTRODUCTION
------------

Intended to make bulk discounts and surcharges that display between the 
Subtotal and Total easier to apply, the Commerce Bulk Discount module provides 
a way to add custom price components to orders without using PHP.  Once an 
order bulk discount price component has been created, it can be implemented by 
creating a Rule triggered by the 'After updating an existing commerce order' 
event. Logic can be applied via Rules conditions and the bulk discount amount 
calculated via a Rules action.  The calculated amount can be passed into the 
provided 'Apply bulk discount to order' Rules action which displays the custom 
discount or surcharge between the order Subtotal and Total fields.

REQUIREMENTS
------------
Drupal 7.x

CONFIGURATION
-------------
See demo at http://youtu.be/Uzh9WFCy6u8

MAINTAINER
----------

creativepragmatic - http://drupal.org/user/177726
