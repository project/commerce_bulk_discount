<?php
/**
 * @file
 * Providing extra functionality for the Bulk Discount UI via views.
 */

/**
 * Implements hook_views_default_views().
 */
function commerce_bulk_discount_views_default_views() {

  $views = array();

  $view = new view();
  $view->name = 'commerce_bulk_discounts';
  $view->description = '';
  $view->tag = 'commerce';
  $view->base_table = 'commerce_bulk_discount';
  $view->human_name = 'Commerce Bulk Discounts (or Surcharges)';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Bulk Discounts/Surcharges';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'create commerce_order entities';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'bulk_discount_id' => 'bulk_discount_id',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'bulk_discount_id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );

  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['label'] = 'Empty';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No bulk discounts (or surcharges) have been created yet';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';

  /* Field: Commerce Bulk Discount (or Surcharge): Commerce bulk discount (or surcharge) ID */
  $handler->display->display_options['fields']['bulk_discount_id']['id'] = 'bulk_discount_id';
  $handler->display->display_options['fields']['bulk_discount_id']['table'] = 'commerce_bulk_discount';
  $handler->display->display_options['fields']['bulk_discount_id']['field'] = 'bulk_discount_id';
  $handler->display->display_options['fields']['bulk_discount_id']['exclude'] = TRUE;

  /* Field: Commerce Bulk Discount (or Surcharge): Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'commerce_bulk_discount';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Name (Internal)';

  /* Field: Commerce Bulk Discount (or Surcharge): Display_title */
  $handler->display->display_options['fields']['display_title']['id'] = 'display_title';
  $handler->display->display_options['fields']['display_title']['table'] = 'commerce_bulk_discount';
  $handler->display->display_options['fields']['display_title']['field'] = 'display_title';
  $handler->display->display_options['fields']['display_title']['label'] = 'Line Item Name';

  /* Field: Commerce Bulk Discount (or Surcharge): Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'commerce_bulk_discount';
  $handler->display->display_options['fields']['type']['field'] = 'type';

  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = 'Edit Link';
  $handler->display->display_options['fields']['nothing']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nothing']['alter']['text'] = 'Edit';
  $handler->display->display_options['fields']['nothing']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nothing']['alter']['path'] = 'admin/commerce/config/bulk-discounts/discount/[bulk_discount_id]/edit';

  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing_1']['id'] = 'nothing_1';
  $handler->display->display_options['fields']['nothing_1']['table'] = 'views';
  $handler->display->display_options['fields']['nothing_1']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing_1']['label'] = 'Delete Link';
  $handler->display->display_options['fields']['nothing_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nothing_1']['alter']['text'] = 'Delete';
  $handler->display->display_options['fields']['nothing_1']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nothing_1']['alter']['path'] = 'admin/commerce/config/bulk-discounts/discount/[bulk_discount_id]/delete';

  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing_2']['id'] = 'nothing_2';
  $handler->display->display_options['fields']['nothing_2']['table'] = 'views';
  $handler->display->display_options['fields']['nothing_2']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing_2']['label'] = 'Operations';
  $handler->display->display_options['fields']['nothing_2']['alter']['text'] = '[nothing]&nbsp;&nbsp;[nothing_1]';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'bulk_discounts_admin_page');
  $handler->display->display_options['path'] = 'admin/commerce/config/bulk-discounts/list';
  $handler->display->display_options['menu']['type'] = 'default tab';
  $handler->display->display_options['menu']['title'] = 'List';
  $handler->display->display_options['menu']['weight'] = '-10';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['tab_options']['type'] = 'normal';
  $handler->display->display_options['tab_options']['title'] = 'Bulk Discounts/Surcharges';
  $handler->display->display_options['tab_options']['description'] = 'Manage bulk discounts/surcharges)';
  $handler->display->display_options['tab_options']['weight'] = '0';
  $handler->display->display_options['tab_options']['name'] = 'management';

  $views[] = $view;

  return $views;
}
