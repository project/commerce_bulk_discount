<?php

/**
 * @file
 * Bulk Discount (or Surcharge) editing UI.
 *
 * We make very little use of the EntityAPI interface for this - preferring
 * instead to use view
 */

/**
 * UI controller.
 */
class CommerceBulkDiscountUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   * 
   * The main reason for overriding is to optimize parent class hook_menu() for
   * entity type administration.
   */
  public function hook_menu() {

    $items = array();
    $id_count = count(explode('/', $this->path));
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ?
      $this->entityInfo['admin ui']['menu wildcard'] : '%' . $this->entityType;

    $items[$this->path] = array(
      'title' => 'Bulk Discounts (or Surcharges)',
      'description' => 'Add, edit and update bulk discounts (or surcharges).',
      'page callback' => 'system_admin_menu_block_page',
      'access arguments' => array('access administration pages'),
      'file path' => drupal_get_path('module', 'system'),
      'file' => 'system.admin.inc',
    );

    // Change the overview menu type for the list of bulk discounts/surcharges.
    $items[$this->path]['type'] = MENU_LOCAL_TASK;

    // Change the add page menu to multiple types of entities.
    $items[$this->path . '/add'] = array(
      'title' => 'Add a bulk discount (or surcharge)',
      'description' => 'Add a new bulk dicount (or surcharge)',
      'page callback'  => 'commerce_bulk_discount_add_page',
      'access callback'  => 'commerce_bulk_discount_access',
      'access arguments' => array('edit'),
      'type' => MENU_NORMAL_ITEM,
      'weight' => 20,
      'file' => 'commerce_bulk_discount.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module']),
    );

    // Add menu items to add each different type of entity.
    foreach (commerce_bulk_discount_get_types() as $type) {
      $items[$this->path . '/add/' . $type->type] = array(
        'title' => 'Add ' . $type->label,
        'page callback' => 'commerce_bulk_discount_form_wrapper',
        'page arguments' => array(commerce_bulk_discount_create(array('type' => $type->type))),
        'access callback' => 'commerce_bulk_discount_access',
        'access arguments' => array('edit', 'edit ' . $type->type),
        'file' => 'commerce_bulk_discount.admin.inc',
        'file path' => drupal_get_path('module', $this->entityInfo['module']),
      );
    }

    // Loading and editing bulk discount (or surcharge) entities.
    $items[$this->path . '/discount/' . $wildcard] = array(
      'page callback' => 'commerce_bulk_discount_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'commerce_bulk_discount_access',
      'access arguments' => array('edit', $id_count + 1),
      'weight' => 0,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file' => 'commerce_bulk_discount.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module']),
    );

    $items[$this->path . '/discount/' . $wildcard . '/edit'] = array(
      'title' => 'Edit',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    );

    $items[$this->path . '/discount/' . $wildcard . '/delete'] = array(
      'title' => 'Delete',
      'page callback' => 'commerce_bulk_discount_delete_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'commerce_bulk_discount_access',
      'access arguments' => array('edit', $id_count + 1),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_INLINE,
      'weight' => 10,
      'file' => 'commerce_bulk_discount.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module']),
    );

    return $items;
  }

  /**
   * Generates markup for the Bulk Discount (or Surplus) Entities page.
   */
  public function addPage() {

    $item = menu_get_item();
    $content = system_admin_menu_block($item);

    if (count($content) == 1) {
      $item = array_shift($content);
      drupal_goto($item['href']);
    }

    return theme('bulk_discount_add_list', array('content' => $content));
  }
}

/**
 * Form callback wrapper: create or edit a bulk discount (or surcharge).
 *
 * @param object $bulk_discount
 *   The bulk discount (or surcharge) object being edited by this form.
 *
 * @see commerce_bulk_discount_edit_form()
 */
function commerce_bulk_discount_form_wrapper($bulk_discount) {
  // Add the breadcrumb for the form's location.
  commerce_bulk_discount_set_breadcrumb();
  return drupal_get_form('commerce_bulk_discount_edit_form', $bulk_discount);
}

/**
 * Form callback wrapper: delete a bulk discount (or surcharge).
 *
 * @param object $bulk_discount
 *   The bulk discount (or surcharge) object being edited by this form.
 *
 * @see commerce_bulk_discount_edit_form()
 */
function commerce_bulk_discount_delete_form_wrapper($bulk_discount) {
  // Add the breadcrumb for the form's location.
  return drupal_get_form('commerce_bulk_discount_delete_form', $bulk_discount);
}

/**
 * Form callback: create or edit a bulk discount (or surcharge).
 *
 * @param object $bulk_discount
 *   The bulk discount (or surcharge) object to edit or create an empty bulk
 *   discount (or surcharge) object with only an bulk discount (or surcharge)
 *   type defined.
 */
function commerce_bulk_discount_edit_form($form, &$form_state, $bulk_discount) {

  // Add the default field elements.
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Bulk Discount (or Surcharge) Title'),
    '#default_value' => isset($bulk_discount->title) ? $bulk_discount->title : '',
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -5,
  );

  // Machine-readable type name.
  $form['machine_name'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($bulk_discount->machine_name) ? $bulk_discount->machine_name : '',
    '#maxlength' => 32,
    '#machine_name' => array(
      'exists' => 'commerce_bulk_discount_get_machine_names',
      'source' => array('title'),
    ),
    '#description' => t('A unique machine-readable name for this bulk discount 
      (or surcharge).  It can only contain lowercase letters, numbers, and 
      underscores.  It also cannot match the machine-name of any existing 
      price component type.'),
  );

  $form['display_title'] = array(
    '#title' => t('Display Label'),
    '#type' => 'textfield',
    '#default_value' => isset($bulk_discount->display_title) ? $bulk_discount->display_title : '',
    '#description' => t('The title of this bulk discount (or surcharge) that 
      appears on the checkout form.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  $form['weight'] = array(
    '#title' => t('Weight'),
    '#type' => 'weight',
    '#default_value' => isset($bulk_discount->weight) ? $bulk_discount->weight : -15,
    '#description' => t('When listed in the checkout form, smaller weights are 
      listed before larger weights. Equal weights are sorted alphabetically. 
      Since the weight of the subtotal is -50, the weight for any discounts (or 
      surcharges) should be lower than that.'),
    '#delta' => 49,
  );

  // Add the field related form elements.
  $form_state['commerce_bulk_discount'] = $bulk_discount;
  field_attach_form('commerce_bulk_discount', $bulk_discount, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save bulk discount (or surcharge)'),
    '#submit' => $submit + array('commerce_bulk_discount_edit_form_submit'),
  );

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'commerce_bulk_discount_edit_form_validate';

  return $form;
}

/**
 * Form API validate callback for the bulk discount (or surcharge) form.
 */
function commerce_bulk_discount_edit_form_validate(&$form, &$form_state) {

  $bulk_discount = $form_state['commerce_bulk_discount'];

  // Notify field widgets to validate their data.
  field_attach_form_validate('commerce_bulk_discount', $bulk_discount, $form, $form_state);
}

/**
 * Form API submit callback for the bulk discount (or surcharge) form.
 *
 * @todo remove hard-coded link
 */
function commerce_bulk_discount_edit_form_submit(&$form, &$form_state) {

  $bulk_discount = entity_ui_controller('commerce_bulk_discount')->entityFormSubmitBuildEntity($form, $form_state);

  // Save the bulk discount (or surcharge) and go back to the list of bulk
  // discounts/surcharges.  Add in created and changed times.
  if ($bulk_discount->is_new = isset($bulk_discount->is_new) ? $bulk_discount->is_new : 0) {
    $bulk_discount->created = time();
  }

  $bulk_discount->changed = time();

  $bulk_discount->save();

  $form_state['redirect'] = 'admin/commerce/config/bulk-discounts';
}

/**
 * Form callback: confirmation form for deleting a discount or surcharge.
 *
 * @param object $bulk_discount
 *   The discount or surcharge to delete
 *
 * @see confirm_form()
 */
function commerce_bulk_discount_delete_form($form, &$form_state, $bulk_discount) {

  $form_state['commerce_bulk_discount'] = $bulk_discount;

  $form['#submit'][] = 'commerce_bulk_discount_delete_form_submit';

  $form = confirm_form(
    $form,
    t('Are you sure you want to delete the discount/surcharge %title?',
    array('%title' => $bulk_discount->title)),
    'admin/commerce/bulk-discounts/discount',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit callback for bulk_discount_delete_form.
 */
function commerce_bulk_discount_delete_form_submit($form, &$form_state) {

  $bulk_discount = $form_state['commerce_bulk_discount'];

  commerce_bulk_discount_delete($bulk_discount);

  drupal_set_message(t('The bulk discount/surcharge %title has been deleted.',
    array('%title' => $bulk_discount->title)));
  watchdog('bulk_discount', 'Deleted bulk discount (or surcharge) %title.',
    array('%title' => $bulk_discount->title));

  $form_state['redirect'] = 'admin/commerce/config/bulk-discounts';
}

/**
 * Page to add Bulk Discount (or Surcharge) Entities.
 *
 * @todo Pass this through a proper theme function
 */
function commerce_bulk_discount_add_page() {
  $controller = entity_ui_controller('commerce_bulk_discount');
  return $controller->addPage();
}

/**
 * Displays a list of available bulk discount (or surcharge) types.
 *
 * @ingroup themeable
 */
function theme_bulk_discount_add_list($variables) {

  $content = $variables['content'];
  $output = '';

  if ($content) {

    $output = '<dl class="bulk-discount-type-list">';

    foreach ($content as $item) {
      $output .= '<dt>' . l($item['title'], $item['href']) . '</dt>';
      $output .= '<dd>' . filter_xss_admin($item['description']) . '</dd>';
    }

    $output .= '</dl>';
  }
  else {
    if (user_access('administer bulk discount types')) {
      $output = '<p>' . t('Bulk Discount (or Surcharge) Entities cannot be added 
        because you have not created any bulk discount (or surcharge) types yet. 
        Go to the <a href="@create-bulk-discount-type">bulk discount (or surcharge) 
        type creation page</a> to add a new bulk discount (or surcharge) type.',
        array(
          '@create-bulk-discount-type' => url('admin/commerce/config/
            bulk-discounts/types/add'),
        )
      ) . '</p>';
    }
    else {
      $output = '<p>' . t('No bulk discount (or surcharge) types have been created 
        yet for you to use.') . '</p>';
    }
  }

  return $output;
}

/**
 * Sets the breadcrumb for administrative bulk discount/surcharge pages.
 */
function commerce_bulk_discount_set_breadcrumb($bulk_discount_types = FALSE) {

  $breadcrumb = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('Commerce'), 'admin/commerce'),
    l(t('Bulk Discounts'), 'admin/commerce/config/bulk-discounts'),
  );

  drupal_set_breadcrumb($breadcrumb);
}
