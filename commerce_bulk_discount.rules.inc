<?php

/**
 * @file
 * Rules integration for Commerce Bulk Discount.
 */


/**
 * Implements hook_rules_action_info().
 */
function commerce_bulk_discount_rules_action_info() {

  $actions = array();

  if (count(commerce_bulk_discounts()) > 0) {
    $actions['commerce_bulk_discount_apply'] = array(
      'label' => t('Apply bulk discount to order'),
      'parameter' => array(
        'commerce_order' => array(
          'type' => 'commerce_order',
          'label' => t('Order'),
        ),
        'bulk_discount' => array(
          'type' => 'text',
          'label' => t('Bulk discount (or surcharge)'),
          'options list' => 'commerce_bulk_discount_options_list',
        ),
        'amount' => array(
          'type' => 'decimal',
          'label' => t('Bulk Discount (or Surcharge) Amount'),
        ),
      ),
      'group' => t('Commerce Bulk Discount'),
    );
  }

  $actions['commerce_bulk_discount_delete_bulk_discount_line_items'] = array(
    'label' => t('Delete all bulk discount (or surcharge) line items from an order'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
    ),
    'group' => t('Commerce Bulk Discount'),
  );

  return $actions;
}
