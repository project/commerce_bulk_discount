<?php

/**
 * @file
 * Bulk Discount/Surcharge type editing UI.
 */

class CommerceBulkDiscountTypeUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage bulk discount (or surcharge) 
      entity types, including adding and removing fields and the display of 
      fields.';
    $items[$this->path]['type'] = MENU_LOCAL_TASK;
    return $items;
  }
}

/**
 * Generates the bulk discount or surcharge type editing form.
 */
function commerce_bulk_discount_type_form($form, &$form_state, $bulk_discount_type, $op = 'edit') {

  if ($op == 'clone') {
    $bulk_discount_type->label .= ' (cloned)';
    $bulk_discount_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $bulk_discount_type->label,
    '#description' => t('The human-readable name of this bulk discount or surcharge type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($bulk_discount_type->type) ? $bulk_discount_type->type : '',
    '#maxlength' => 32,
    '#machine_name' => array(
      'exists' => 'commerce_bulk_discount_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this bulk discount 
      (or surcharge) type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save bulk discount (or surcharge) type'),
    '#weight' => 40,
  );

  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function commerce_bulk_discount_type_form_submit(&$form, &$form_state) {
  $bulk_discount_type = entity_ui_form_submit_build_entity($form, $form_state);
  $bulk_discount_type->save();
  $form_state['redirect'] = 'admin/commerce/config/bulk-discounts/types';
}

/**
 * Form API submit callback for the delete button.
 */
function commerce_bulk_discount_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/commerce/config/bulk-discounts/types/manage/' .
    $form_state['commerce_bulk_discount_type']->type . '/delete';
}
