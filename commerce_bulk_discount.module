<?php

/**
 * @file
 * Module for the Commerce Bulk Discount (or Surcharge) Entity
 */

/**
 * Implements hook_entity_info().
 */
function commerce_bulk_discount_entity_info() {

  $entities = array();

  $entities['commerce_bulk_discount'] = array(
    'label' => t('Commerce Bulk Discount (or Surcharge)'),
    'entity class' => 'CommerceBulkDiscount',
    'controller class' => 'CommerceBulkDiscountController',
    'base table' => 'commerce_bulk_discount',
    'fieldable' => TRUE,
    'entity keys' => array(
      'id' => 'bulk_discount_id',
      'bundle' => 'type',
    ),
    // Bundle keys tell the FieldAPI how to extract information from the bundle
    // objects.
    'bundle keys' => array(
      'bundle' => 'type',
    ),
    // Bundles are defined by the bulk discount (or surcharge) types below.
    'bundles' => array(),
    'load hook' => 'commerce_bulk_discount_load',
    'label callback' => 'entity_class_label',
    'uri callback' => 'entity_class_uri',
    'creation callback' => 'commerce_bulk_discount_create',
    'access callback' => 'commerce_bulk_discount_access',
    'module' => 'commerce_bulk_discount',
    // The information below is used by the CommerceBulkDiscountUIController
    // (which extends the EntityDefaultUIController).
    'admin ui' => array(
      'path' => 'admin/commerce/config/bulk-discounts',
      'file' => 'commerce_bulk_discount.admin.inc',
      'controller class' => 'CommerceBulkDiscountUIController',
    ),
  );

  $entities['commerce_bulk_discount_type'] = array(
    'label' => t('Commerce Bulk Discount (or Surcharge) Type'),
    'entity class' => 'CommerceBulkDiscountType',
    'controller class' => 'CommerceBulkDiscountTypeController',
    'base table' => 'commerce_bulk_discount_type',
    'fieldable' => FALSE,
    'bundle of' => 'commerce_bulk_discount',
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'id',
      'name' => 'type',
      'label' => 'label',
    ),
    'access callback' => 'commerce_bulk_discount_type_access',
    'module' => 'commerce_bulk_discount',
    // Enable the entity API's admin UI.
    'admin ui' => array(
      'path' => 'admin/commerce/config/bulk-discounts/types',
      'file' => 'commerce_bulk_discount_type.admin.inc',
      'controller class' => 'CommerceBulkDiscountTypeUIController',
    ),
  );

  return $entities;
}

/**
 * Implements hook_commerce_price_component_type_info().
 */
function commerce_bulk_discount_commerce_price_component_type_info() {

  $components = array();

  // Load all bulk discount and surcharge entities.
  $bulk_discounts = commerce_bulk_discounts();

  // Define price components for all bulk discounts or surcharges.
  foreach ($bulk_discounts as $bulk_discount) {
    $components[$bulk_discount->machine_name] = array(
      'title' => $bulk_discount->title,
      'display_title' => $bulk_discount->display_title,
      'weight' => $bulk_discount->weight,
    );
  }

  return $components;
}

/**
 * Implements hook_commerce_line_item_type_info().
 */
function commerce_bulk_discount_commerce_line_item_type_info() {

  $line_item_types = array();

  $line_item_types['bulk_discount'] = array(
    'name' => t('Bulk Discount (or Surcharge)'),
    'description' => t('References a bulk discount (or surcharge) and 
      displays the bulk discount (or surcharge) amount and selected bulk 
      discount (or surcharge) title.'),
    'product' => FALSE,
    'add_form_submit_value' => t('Add bulk discount (or surcharge)'),
    'base' => 'commerce_bulk_discount_line_item',
  );

  return $line_item_types;
}

/**
 * Bulk Discount (or Surcharge) line item type callbacks.
 */

/**
 * Returns the display title of a bulk discount (or surcharge) line item.
 */
function commerce_bulk_discount_line_item_title($line_item) {

  // Extract machine name of bulk discount (or surcharge) from order bulk
  // discount (or surcharge) line item.
  $language = $line_item->commerce_unit_price['#language'];
  $machine_name = $line_item->commerce_unit_price[$language][0]['data']['components'][0]['name'];

  // Load details of commerce bulk discount (or surcharge) price component.
  $bulk_discount = commerce_price_component_type_load($machine_name);

  // Return display title of bulk discount (or surcharge).
  return ($bulk_discount['display_title']);
}

/**
 * Empty bulk discount/surcharge add line item form array.
 *
 * Bulk discounts (or surcharges) are added automatically (via Rules) so
 * an empty form array is returned.
 */
function commerce_bulk_discount_line_item_add_form($element, &$form_state) {
  $form = array();
  return $form;
}

/**
 * Action: Apply bulk discount (or surcharge) to order.
 */
function commerce_bulk_discount_apply($order, $bulk_discount_id, $amount = 0) {

  $bulk_discount = commerce_bulk_discount_load($bulk_discount_id);

  // Delete any existing bulk discount (or surcharge) line items from the order.
  commerce_bulk_discount_delete_bulk_discount_line_items($order, TRUE);

  $line_item = commerce_bulk_discount_line_item_new($order, $amount, $bulk_discount->machine_name);

  commerce_line_item_save($line_item);

  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  $order_wrapper->commerce_line_items[] = $line_item;

  commerce_order_save($order);
}

/**
 * Creates a new bulk discount (or surcharge) line item.
 *
 * @param object $order
 *   The order the bulk discount (or surcharge) line item belongs to.
 * @param float $amount
 *   The value of the bulk discount (or surcharge) line item's unit price.
 * @param string $machine_name
 *   The machine name of the bulk discount (or surcharge).
 * @param array $data
 *   An array value to initialize the line item's data array with.
 * @param string $type
 *   The name of the line item type being created; defaults to 'bulk_discount'.
 *
 * @return object
 *   The order line item for the bulk discount (or surcharge).
 */
function commerce_bulk_discount_line_item_new($order, $amount, $machine_name,
    $data = array(), $type = 'bulk_discount') {

  // Ensure a default bulk discount (or surcharge) line item type.
  if (empty($type)) {
    $type = 'bulk_discount';
  }

  // Create the new line item.
  $line_item = entity_create('commerce_line_item', array(
    'type' => $type,
    'order_id' => $order->order_id,
    'line_item_label' => 'BULK_DISCOUNT_SURCHARGE',
    'quantity' => 1,
    'data' => $data,
  ));

  $line_item->commerce_unit_price = array(
    'und' => array(
      '0' => array(
        'amount' => commerce_currency_decimal_to_amount($amount,
          commerce_default_currency()),
        'currency_code' => commerce_default_currency(),
        'data' => $data,
      ),
    ),
  );

  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

  if (!is_null($line_item_wrapper->commerce_unit_price->value())) {
    // Add the base price to the components array.
    if (!commerce_price_component_load($line_item_wrapper->commerce_unit_price->value(), 'base_price')) {
      $line_item_wrapper->commerce_unit_price->data = commerce_price_component_add(
        $line_item_wrapper->commerce_unit_price->value(),
        $machine_name,
        $line_item_wrapper->commerce_unit_price->value(),
        TRUE,
        FALSE
      );
    }
  }

  // Return the line item.
  return $line_item;
}

/**
 * Implements hook_module_implements_alter().
 */
function commerce_bulk_discount_module_implements_alter(&$implementations, $hook) {

  // To allow the default rules defined by this module to be overridden by other
  // modules (such as Features produced modules), we need to ensure that this
  // module's hook_default_rules_configuration() is invoked before theirs.
  if ($hook == 'default_rules_configuration') {

    // Extract this module's entry from the hook implementations array.
    $group = $implementations['commerce_bulk_discount'];
    unset($implementations['commerce_bulk_discount']);

    // And re-add it by prepending it back onto the array.
    $implementations = array('commerce_bulk_discount' => $group) + $implementations;
  }
}

/**
 * Returns an array containing all bulk discount entities.
 *
 * @return array
 *   An array of bulk discount (or surcharge) objects indexed by their ids. When
 *   no results are found, an empty array is returned.
 */
function commerce_bulk_discounts() {
  $bulk_discounts = entity_load('commerce_bulk_discount');
  return $bulk_discounts;
}

/**
 * Deletes all bulk discount and surcharge line items on an order.
 *
 * @param object $order
 *   The order object to delete the discount and surcharge line items from.
 */
function commerce_bulk_discount_delete_bulk_discount_line_items($order) {

  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  // When deleting more than one line item, metadata_wrapper will give problems
  // if deleting while looping through the line items. So first remove from
  // order and then delete the line items.
  $line_item_ids = array();

  foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {

    // If this line item is a bulk discount (or surcharge) line item...
    if ($line_item_wrapper->type->value() == 'bulk_discount') {

      // Store its ID for later deletion and remove the reference from the line
      // item reference field.
      $line_item_ids[] = $line_item_wrapper->line_item_id->value();
      $order_wrapper->commerce_line_items->offsetUnset($delta);
    }
  }

  // If we found any bulk discount (or surcharge) line items...
  if (!empty($line_item_ids)) {

    // First save the order to update the line item reference field value.
    commerce_order_save($order);

    // Then delete the line items.
    commerce_line_item_delete_multiple($line_item_ids);
  }
}

/**
 * Get a list of all commerce bulk discounts and surcharges available.
 */
function commerce_bulk_discount_options_list() {

  $bulk_discounts = commerce_bulk_discounts();

  $bulk_discount_options = array();

  foreach ($bulk_discounts as $bulk_discount) {
    $bulk_discount_options[$bulk_discount->bulk_discount_id] = $bulk_discount->title;
  }

  return $bulk_discount_options;
}

/**
 * Implements hook_entity_info_alter().
 *
 * We are adding the info about the bulk discount/surcharge types via a
 * hook to avoid a recursion issue as loading the bulk discount/surcharge
 * types requires the entity info as well.
 *
 * @todo This needs to be improved
 */
function commerce_bulk_discount_entity_info_alter(&$entity_info) {

  foreach (commerce_bulk_discount_get_types() as $type => $info) {
    $entity_info['commerce_bulk_discount']['bundles'][$type] = array(
      'label' => $info->label,
      'admin' => array(
        'path' => 'admin/commerce/config/bulk-discounts/types/manage/%commerce_bulk_discount_type',
        'real path' => 'admin/commerce/config/bulk-discounts/types/manage/' . $type,
        'bundle argument' => 5,
        'access arguments' => array('administer bulk discount types'),
      ),
    );
  }
}

/**
 * Implements hook_permission().
 */
function commerce_bulk_discount_permission() {

  // We set up permisssions to manage entity types, manage all entities and the
  // permissions for each individual entity.
  $permissions = array(
    'administer bulk discounts' => array(
      'title' => t('Administer bulk discounts and surcharges'),
      'description' => t('Edit and delete all order discounts and surcharges'),
    ),
    'administer bulk discount types' => array(
      'title' => t('Administer bulk discount and surcharge types'),
      'description' => t('Create and delete fields for bulk discount or 
        surcharge types, and set their permissions.'),
    ),
  );

  // Generate permissions per order discount/surplus
  foreach (commerce_bulk_discount_get_types() as $type) {
    $type_name = check_plain($type->type);
    $permissions += array(
      "edit any $type_name bulk discount" => array(
        'title' => t('%type_name: Edit any bulk discount or surcharge',
          array('%type_name' => $type->label)),
      ),
      "view any $type_name bulk discount" => array(
        'title' => t('%type_name: View any bulk discount or surcharge',
          array('%type_name' => $type->label)),
      ),
    );
  }

  return $permissions;
}

/**
 * Determines whether the given user has access to a bulk discount/surcharge.
 *
 * @param string $op
 *   The operation being performed. One of 'view', 'update', 'create', 'delete'
 *   or just 'edit' (being the same as 'create' or 'update').
 * @param object $bulk_discount
 *   Optionally a bulk discount/surcharge or a bulk discount/surcharge
 *   type to check access for. If nothing is given, access for all order
 *   bulk discounts/surcharges is determined.
 * @param object $account
 *   The user to check for. Leave it to NULL to check for the global user.
 *
 * @return bool
 *   Whether access is allowed or not.
 */
function commerce_bulk_discount_access($op, $bulk_discount = NULL, $account = NULL) {

  if (user_access('administer bulk discounts', $account)) {
    return TRUE;
  }

  if (isset($bulk_discount) && $type_name = $bulk_discount->type) {
    $op = ($op == 'view') ? 'view' : 'edit';
    if (user_access("$op any $type_name bulk discount", $account)) {
      return TRUE;
    }
  }

  return FALSE;
}

/**
 * Access callback for the entity API.
 */
function commerce_bulk_discount_type_access($op, $type = NULL, $account = NULL) {
  return user_access('administer bulk discount types', $account);
}

/**
 * Gets an array of all bulk discount/surcharge types, keyed by the type name.
 *
 * @param string $type_name
 *   If set, the type with the given name is returned.
 *
 * @return CommerceBulkDiscountType[]
 *   Depending whether $type isset, an array of bulk discount/surcharge
 *   types or a single one.
 */
function commerce_bulk_discount_get_types($type_name = NULL) {
  // entity_load will get the Entity controller for our order discount/surcharge
  // entity and call the load function of that object - we are loading entities
  // by name here.
  $types = entity_load_multiple_by_name('commerce_bulk_discount_type',
    isset($type_name) ? array($type_name) : FALSE);
  return isset($type_name) ? reset($types) : $types;
}

/**
 * Menu argument loader; Load a bulk discount/surcharge type by string.
 *
 * @param string $type
 *   The machine-readable name of a bulk discount/surcharge type to load.
 *
 * @return array
 *   An order bulk/discount type array or FALSE if $type does not exist.
 */
function commerce_bulk_discount_type_load($type) {
  return commerce_bulk_discount_get_types($type);
}

/**
 * Returns array of all bulk discount/surcharge machine names.
 * 
 * Results keyed by machine name.
 *
 * @param string $machine_name
 *   If set, the machine_name with the given name is returned.
 *
 * @return CommerceBulkDiscount[]
 *   Depending whether $machine_name is set, an array of order bulk 
 *   discount/surcharge machine names or a single one.
 */
function commerce_bulk_discount_get_machine_names($machine_name = NULL) {

  // Check to make sure a bulk discount/surcharge with the same machine name
  // does not already exist.
  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'commerce_bulk_discount')
    ->propertyCondition('machine_name', $machine_name, '=');

  $count = $query->count()->execute();

  if ($count > 0) {
    return TRUE;
  }

  // Check to make sure a price component type with the same machine name does
  // not already exist.
  if (in_array($machine_name, array_keys(commerce_price_component_types()))) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Fetch a bulk discount/surcharge object.
 * 
 * Ensures that the wildcard chosen in the bulk discount/surcharge entity
 * definition fits the function name here.
 *
 * @param int $bulk_discount_id
 *   Integer specifying the bulk discount id.
 * @param bool $reset
 *   A boolean indicating that the internal cache should be reset.
 *
 * @return object
 *   A fully-loaded $bulk_discount object or FALSE if it cannot be loaded.
 *
 * @see commerce_bulk_discount_load_multiple()
 */
function commerce_bulk_discount_load($bulk_discount_id = NULL, $reset = FALSE) {
  $bulk_discount_ids = (isset($bulk_discount_id) ? array($bulk_discount_id) : array());
  $bulk_discount = commerce_bulk_discount_load_multiple($bulk_discount_ids, array(), $reset);
  return $bulk_discount ? reset($bulk_discount) : FALSE;
}


/**
 * Load multiple bulk discounts/surcharges based on certain conditions.
 *
 * @param array $bulk_discount_ids
 *   An array of bulk discount IDs.
 * @param array $conditions
 *   An array of conditions to match against the {bulk_discount} table.
 * @param bool $reset
 *   A boolean indicating that the internal cache should be reset.
 *
 * @return array
 *   An array of bulk discount objects, indexed by bulk_discount_id.
 *
 * @see entity_load()
 * @see bulk_discount_load()
 */
function commerce_bulk_discount_load_multiple($bulk_discount_ids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('commerce_bulk_discount', $bulk_discount_ids, $conditions, $reset);
}

/**
 * Deletes a bulk discount/surcharge.
 */
function commerce_bulk_discount_delete(CommerceBulkDiscount $bulk_discount) {
  $bulk_discount->delete();
}

/**
 * Delete multiple bulk discounts/surcharges.
 *
 * @param array $bulk_discount_ids
 *   An array of order discount (or surcharge) IDs.
 */
function commerce_bulk_discount_delete_multiple(array $bulk_discount_ids) {
  entity_get_controller('commerce_bulk_discount')->delete($bulk_discount_ids);
}

/**
 * Create a bulk discount (or surcharge) object.
 */
function commerce_bulk_discount_create($values = array()) {
  return entity_get_controller('commerce_bulk_discount')->create($values);
}

/**
 * Saves a bulk discount (or surcharge) to the database.
 *
 * @param object $bulk_discount
 *   The bulk discount/surcharge object.
 */
function commerce_bulk_discount_save(CommerceBulkDiscount $bulk_discount) {
  return $bulk_discount->save();
}

/**
 * Saves a bulk discount/surcharge type to the db.
 */
function commerce_bulk_discount_type_save(CommerceBulkDiscountType $type) {
  $type->save();
}

/**
 * Deletes a bulk discount/surcharge type from the db.
 */
function commerce_bulk_discount_type_delete(CommerceBulkDiscountType $type) {
  $type->delete();
}

/**
 * URI callback for bulk discounts/surcharges.
 */
function commerce_bulk_discount_uri(CommerceBulkDiscount $bulk_discount) {
  return array(
    'path' => 'discount/' . $bulk_discount->bulk_discount_id,
  );
}

/**
 * Implements hook_views_api().
 */
function commerce_bulk_discount_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'commerce_bulk_discount') . '/views',
  );
}

/**
 * Implements hook_theme().
 */
function commerce_bulk_discount_theme() {
  return array(
    'bulk_discount_add_list' => array(
      'variables' => array('commerce' => array()),
      'file' => 'commerce_bulk_discount.admin.inc',
    ),
  );
}

/**
 * Implements hook_menu_local_tasks_alter().
 */
function commerce_bulk_discount_menu_local_tasks_alter(&$data, $router_item, $root_path) {
  // Add action link 'admin/commerce/config/bulk-discounts/add' on
  // 'admin/commerce/config/bulk-discounts'.
  if ($root_path == 'admin/commerce/config/bulk-discounts') {
    $item = menu_get_item('admin/commerce/config/bulk-discounts/add');
    if ($item['access']) {
      $data['actions']['output'][] = array(
        '#theme' => 'menu_local_action',
        '#link' => $item,
      );
    }
  }
}

/**
 * The class used for bulk discount/surcharge entities
 */
class CommerceBulkDiscount extends Entity {

  /**
   * CommerceBulkDiscount class constructor.
   */
  public function __construct($values = array()) {
    parent::__construct($values, 'commerce_bulk_discount');
  }

  /**
   * Returns CommerceBulkDiscount default label.
   */
  protected function defaultLabel() {
    return $this->title;
  }

  /**
   * Returns CommerceBulkDiscount defual URI.
   */
  protected function defaultUri() {
    return array('path' => 'discount/' . $this->bulk_discount_id);
  }
}

/**
 * The class used for bulk discount type entities
 */
class CommerceBulkDiscountType extends Entity {

  public $type;
  public $label;

  /**
   * CommerceBulkDiscountType class constructor.
   */
  public function __construct($values = array()) {
    parent::__construct($values, 'commerce_bulk_discount_type');
  }
}

/**
 * The Controller for CommerceBulkDiscount entities
 */
class CommerceBulkDiscountController extends EntityAPIController {

  /**
   * CommerceBulkDiscountController class constructor.
   */
  public function __construct($entity_type) {
    parent::__construct($entity_type);
  }

  /**
   * Create a new bulk discount or surcharge.
   * 
   * We first set up the values that are specific to our bulk discount/surcharge
   * schema but then also go through the EntityAPIController function.
   * 
   * @param array $values
   *   The machine-readable type of the bulk discount/surcharge.
   *
   * @return object
   *   A bulk discount/surcharge object with all default fields initialized.
   */
  public function create(array $values = array()) {

    // Add values that are specific to our Bulk Discount (or Surcharge).
    $values += array(
      'bulk_discount_id' => '',
      'is_new' => TRUE,
      'title' => '',
      'machine_name' => '',
      'display_title' => '',
      'weight' => -40,
      'created' => '',
      'changed' => '',
    );

    $bulk_discount = parent::create($values);

    return $bulk_discount;
  }
}

/**
 * The Controller for Bulk Discount entities
 */
class CommerceBulkDiscountTypeController extends EntityAPIControllerExportable {

  /**
   * CommerceBulkDiscountTypeController class constructor.
   */
  public function __construct($entity_type) {
    parent::__construct($entity_type);
  }

  /**
   * Returns bulk discount/surcharge type object.
   * 
   * We first set up the values that are specific to our bulk discount/surcharge
   *   type schema but then also go through the EntityAPIController function.
   *
   * @param array $values
   *   The machine-readable array of values for the bulk discount/surcharge
   *   type.
   *
   * @return object
   *   A bulk discount/surcharge type object with all default fields 
   *   initialized.
   */
  public function create(array $values = array()) {

    // Add values that are specific to our Discount/Surcharge
    $values += array(
      'id' => '',
      'is_new' => TRUE,
    );

    $bulk_discount_type = parent::create($values);

    return $bulk_discount_type;
  }
}
